import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AddPackagePage } from '../pages/add-package/add-package';
import { AthletesPage } from '../pages/athletes/athletes';
import { AthleteProfilePage } from '../pages/athlete-profile/athlete-profile';
import { LanesPage } from '../pages/lanes/lanes';
import { LoginPage } from '../pages/login/login';
import { QrScannerPage } from '../pages/qr-scanner/qr-scanner';
import { SessionDetailsPage } from '../pages/session-details/session-details';
import { SessionsPage } from '../pages/sessions/sessions';
import { WaitingListPage } from '../pages/waiting-list/waiting-list';
import { WorkoutDetailsPage } from '../pages/workout-details/workout-details';
import { WorkoutsPage } from '../pages/workouts/workouts';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { LevelsPage } from '../pages/levels/levels';
import { FocusPage } from '../pages/focus/focus';
import { EventPage } from '../pages/event/event';
import { ScannedAthletePage } from '../pages/scanned-athlete/scanned-athlete';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    AddPackagePage,
    AthletesPage,
    AthleteProfilePage,
    LanesPage,
    LoginPage,
    QrScannerPage,
    SessionDetailsPage,
    SessionsPage,
    WaitingListPage,
    WorkoutDetailsPage,
    WorkoutsPage,
    LevelsPage,
    FocusPage,
    EventPage,
    ScannedAthletePage   
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    AddPackagePage,
    AthletesPage,
    AthleteProfilePage,
    LanesPage,
    LoginPage,
    QrScannerPage,
    SessionDetailsPage,
    SessionsPage,
    WaitingListPage,
    WorkoutDetailsPage,
    WorkoutsPage,LevelsPage,
    FocusPage,
    EventPage,
    ScannedAthletePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
