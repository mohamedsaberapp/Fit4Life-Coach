import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { AthletesPage } from '../athletes/athletes';
import { QrScannerPage } from '../qr-scanner/qr-scanner';
import { WorkoutsPage } from '../workouts/workouts';
import { SessionsPage } from '../sessions/sessions';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = AthletesPage;
  tab2Root = WorkoutsPage;
  tab3Root = SessionsPage;
  tab4Root = QrScannerPage;
  constructor() {

  }
}
