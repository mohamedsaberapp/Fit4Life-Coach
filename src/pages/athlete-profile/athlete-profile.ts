import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { AddPackagePage } from '../add-package/add-package';

/**
 * Generated class for the AthleteProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-athlete-profile',
  templateUrl: 'athlete-profile.html',
})
export class AthleteProfilePage {
  hidden: boolean=false;
  choice:any='sessions';
  paid:any="Not Paid";
  myColor:any="danger";
  constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl:AlertController,private modalCtrl:ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AthleteProfilePage');
  }
  addNote(){
    let alert = this.alertCtrl.create({
      title: 'Add  Note',
      inputs: [
        {
          name: 'title',
          placeholder: 'Title'
        },
        {
          name: 'description',
          placeholder: 'Description'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass:'dark-btn',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add',
          cssClass:'dark-btn',
          handler: data => {
            console.log(data);
          }
        }
      ]
    });
    alert.present();
  }

  addPackage(){
    let opts={cssClass:'addPackage',enableBackdropDismiss:true};
    let modal = this.modalCtrl.create(AddPackagePage,{},opts);
    modal.present();
  }
  MarkAsPaid(x){
    console.log(x);
    this.paid="Paid";
    this.hidden=true;
    this.myColor='green';
  }

}
