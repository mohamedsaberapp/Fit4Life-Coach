import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AthleteProfilePage } from './athlete-profile';

@NgModule({
  declarations: [
    AthleteProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(AthleteProfilePage),
  ],
})
export class AthleteProfilePageModule {}
