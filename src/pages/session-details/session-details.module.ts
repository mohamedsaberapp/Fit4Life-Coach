import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SessionDetailsPage } from './session-details';

@NgModule({
  declarations: [
    SessionDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(SessionDetailsPage),
  ],
})
export class SessionDetailsPageModule {}
