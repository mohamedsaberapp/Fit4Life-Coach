import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SessionDetailsPage } from '../session-details/session-details';
import { EventPage } from '../event/event';

/**
 * Generated class for the SessionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sessions',
  templateUrl: 'sessions.html',
})
export class SessionsPage {

  branch: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.branch='Branchone';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SessionsPage');
  }
  viewSession(){
    this.navCtrl.push(SessionDetailsPage);
  }
  viewEvent(){
    this.navCtrl.push(EventPage);
  }

}
