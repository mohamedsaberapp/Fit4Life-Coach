import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScannedAthletePage } from './scanned-athlete';

@NgModule({
  declarations: [
    ScannedAthletePage,
  ],
  imports: [
    IonicPageModule.forChild(ScannedAthletePage),
  ],
})
export class ScannedAthletePageModule {}
