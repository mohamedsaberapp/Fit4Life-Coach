import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { AddPackagePage } from '../add-package/add-package';
import { QrScannerPage } from '../qr-scanner/qr-scanner';

/**
 * Generated class for the ScannedAthletePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scanned-athlete',
  templateUrl: 'scanned-athlete.html',
})
export class ScannedAthletePage {

  hidden: boolean=false;
  choice:any='sessions';
  paid:any="Not Paid";
  myColor:any="danger";
  no_of_sessions:number=5;
  constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl:AlertController,private modalCtrl:ModalController) {
    this.no_of_sessions=5;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScannedAthletePage');
  }
  addNote(){
    let alert = this.alertCtrl.create({
      title: 'Add  Note',
      inputs: [
        {
          name: 'title',
          placeholder: 'Title'
        },
        {
          name: 'description',
          placeholder: 'Description'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass:'dark-btn',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add',
          cssClass:'dark-btn',
          handler: data => {
            console.log(data);
          }
        }
      ]
    });
    alert.present();
  }

  addPackage(){
    let opts={cssClass:'addPackage',enableBackdropDismiss:true};
    let modal = this.modalCtrl.create(AddPackagePage,{},opts);
    modal.present();
  }
  MarkAsPaid(x){
    console.log(x);
    this.paid="Paid";
    this.hidden=true;
    this.myColor='green';
  }
  admitSession(){
    if(this.no_of_sessions>0){
      this.no_of_sessions--;
      this.navCtrl.push(QrScannerPage);
    }else{
      alert('This Athelete ran out of sessions, please ask him to renew his subscription !');
    }
  }

}
