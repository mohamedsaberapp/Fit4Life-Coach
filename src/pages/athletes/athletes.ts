import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AthleteProfilePage } from '../athlete-profile/athlete-profile';

/**
 * Generated class for the AthletesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-athletes',
  templateUrl: 'athletes.html',
})
export class AthletesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AthletesPage');
  }
  view(x){
    console.log(x);
    this.navCtrl.push(AthleteProfilePage,{data:x});
  }
  onInput(e){
    console.log(e);
  }
  onCancel(e){
    console.log(e);
  }
}
