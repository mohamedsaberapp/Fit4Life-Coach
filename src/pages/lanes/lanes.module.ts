import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LanesPage } from './lanes';

@NgModule({
  declarations: [
    LanesPage,
  ],
  imports: [
    IonicPageModule.forChild(LanesPage),
  ],
})
export class LanesPageModule {}
